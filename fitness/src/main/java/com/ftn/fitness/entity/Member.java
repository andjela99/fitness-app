package com.ftn.fitness.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="member", catalog="fitness")
public class Member implements Serializable  {
	
	
	private Integer id;
	private Integer age;
	private Integer weight;
	private Integer height;
	private String desires;
	private Integer absenceCounter;
	private String phoneNumber;
	private Double BMI;
	private Integer userId;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "AGE", nullable = false, length = 45)
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
	@Column(name = "WEIGHT", nullable = false, length = 45)
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	
	@Column(name = "HEIGHT", nullable = false, length = 45)
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	
	@Column(name = "DESIRES", nullable = false, length = 45)
	public String getDesires() {
		return desires;
	}
	public void setDesires(String desires) {
		this.desires = desires;
	}
	
	@Column(name = "ABSENCE_COUNTER", nullable = false, length = 45)
	public Integer getAbsenceCounter() {
		return absenceCounter;
	}
	public void setAbsenceCounter(Integer absenceCounter) {
		this.absenceCounter = absenceCounter;
	}
	
	@Column(name = "PHONE_NUMBER", nullable = false, length = 45)
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Column(name = "BMI", nullable = false, length = 45)
	public Double getBMI() {
		return BMI;
	}
	public void setBMI(Double i) {
		BMI = i;
	}
	@Column(name = "USER_ID", nullable = false, length = 45)
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public Member() {}
	
	public Member(Member member) {
		super();
		this.age = member.getAge();
		this.weight = member.getWeight();
		this.height = member.getHeight();
		this.desires = member.getDesires();
		this.absenceCounter = member.getAbsenceCounter();
		this.BMI = member.getBMI();
		this.phoneNumber = member.getPhoneNumber();
		
		this.userId = member.getUserId();
	}
	
	
	
	

}
