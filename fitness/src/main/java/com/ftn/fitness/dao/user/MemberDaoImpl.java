package com.ftn.fitness.dao.user;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.fitness.dao.AbstractGenericDao;
import com.ftn.fitness.entity.Member;
import com.ftn.fitness.entity.User;

@Repository
@Transactional
public class MemberDaoImpl  extends AbstractGenericDao<Member, Integer> implements MemberDao  {
	
	@Autowired
	public MemberDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@Override
	public Member addMember(Member member) {
		member.setAbsenceCounter(0);
		member.setBMI(0.0);
		insert(member);
		
		return member;
	}

	

}
