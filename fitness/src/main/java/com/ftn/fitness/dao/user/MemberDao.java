package com.ftn.fitness.dao.user;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.fitness.dao.AbstractGenericDao;
import com.ftn.fitness.dao.GenericDao;
import com.ftn.fitness.entity.Member;
import com.ftn.fitness.entity.User;

@Repository
@Transactional
public interface MemberDao  extends GenericDao<Member, Integer> {

	Member addMember(Member member);

	

}
