package com.ftn.fitness.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ftn.fitness.entity.Member;
import com.ftn.fitness.entity.User;
import com.ftn.fitness.service.UserService;
import org.springframework.http.MediaType;

@RestController
@CrossOrigin
public class LoginController {
	
	@Autowired
	private UserService userService;
	
	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(LoginController.class);

    @RequestMapping(value = "/res", method = RequestMethod.GET)
    //@PreAuthorize("hasRole('ROLE_USER')")
    //@JsonView(UserView.FullView.class)
    public User getUser()
            //@RequestHeader(name = "Authorization") String jwtToken, //just for Swagger
           // @AuthenticationPrincipal User user) {
    		{
    	System.out.println("Pogodio si me...");
        return null;//user;
    }
    
    
    @RequestMapping(value = "/login/{username}/{password}")
	public ResponseEntity<?> findUser(@PathVariable("username") String username,
			@PathVariable("password") String password) {
		//BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		return new ResponseEntity<User>(userService.findUser(username, password), HttpStatus.OK);

	}
    @RequestMapping(value = "/signup/addUser/", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
  	public ResponseEntity<?> addUser(@RequestBody User user) {
  		//BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
  		
  		return new ResponseEntity<User>(userService.addUser(user), HttpStatus.OK);

  	}
    
    
    

}

