package com.ftn.fitness.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.fitness.dao.user.UserDao;
import com.ftn.fitness.entity.User;
import com.ftn.fitness.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserDao userDao;

	@Override
	public User findUser(String username, String password) {
		return userDao.findUser(username,password);
	}

	@Override
	public User findUserByUsername(String username) {
		return userDao.findUserByUsername(username);
	}

	@Override
	public User addUser(User user) {
		return userDao.addUser(user);
	}

}
