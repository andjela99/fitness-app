package com.ftn.fitness.service;


import com.ftn.fitness.entity.Member;
import com.ftn.fitness.entity.User;

public interface UserService {

	User findUser(String username, String password);

	User findUserByUsername(String username);

	User addUser(User user);



}
