package com.ftn.fitness.service;

import com.ftn.fitness.entity.Member;
import com.ftn.fitness.entity.User;

public interface MemberService {

	Member addMember(Member member);

}
