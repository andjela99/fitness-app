import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { MemberHomePageComponent } from './member-home-page/member-home-page.component';
import { AdminHomePageComponent } from './admin-home-page/admin-home-page.component';
import { TrainerHomePageComponent } from './trainer-home-page/trainer-home-page.component';
import { NutritionistHomePageComponent } from './nutritionist-home-page/nutritionist-home-page.component';

const routes: Routes = [
  {path: 'loggedMember', component: MemberHomePageComponent},
  {path: 'loggedAdmin', component: AdminHomePageComponent},
  {path: 'loggedTrainer', component: TrainerHomePageComponent},
  {path: 'loggedNutritionist', component: NutritionistHomePageComponent},

  {path: '', component: LoginComponent},
  {path: 'signup', component: SignupComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
