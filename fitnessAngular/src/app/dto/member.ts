export class Member{
    phoneNumber: String
    height: Number
    weight: Number
    absenceCounter: Number
    BMI: Number
    age: Number
    desires: String
    userId: Number
    membershipId: Number
}
