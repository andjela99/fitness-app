export class User{
    id: Number;
    username: String;
    password: String;
    fullName: String;
    email: String;
}