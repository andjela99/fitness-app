import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Member } from '../dto/member';
import { User } from '../dto/user';
import { Endpoint } from '../util/endpoints-enum';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  fullName: String
  email: String
  phoneNumber: String
  height: Number
  weight: Number
  age: Number
  username: String
  password: String
  desires: String
  endpoint = Endpoint;

  constructor(private router: Router,private http: HttpClient) { }

  ngOnInit(): void {
  }

  
  signUp(){
    let member: Member = new Member
    let user: User = new User
    user.username = this.username
    user.password = this.password
    user.fullName = this.fullName
    user.email = this.email
    member.phoneNumber =  this.phoneNumber
    member.weight = this.weight
    member.height = this.height
    member.desires = this.desires

    member.age = this.age


    const headers = { 'content-type': 'application/json'}  // da bi odgovaralo json-u
    const body=JSON.stringify(user);   //konverzija objekta subscriber u json
    
    let options = { headers: headers };

  

        this.http.post<any>(this.endpoint.ANDJELA_DEV+'fitness/signup/addUser/', body, options).pipe(
          ).subscribe(res => {
   
            member.userId = res["id"]
            
            const body1=JSON.stringify(member);
              this.http.post<any>(this.endpoint.ANDJELA_DEV+'fitness/signup/addMember/', body1, options).pipe(
                map(returnedPersonId => {
                  //after sign up redirect to login
                  if(confirm("Successfully created account.")) {
                    this.router.navigate([""]);}

})
).subscribe()}
  )}
}
