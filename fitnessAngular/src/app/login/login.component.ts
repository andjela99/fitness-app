import { SlicePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Endpoint } from '../util/endpoints-enum';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  endpoint = Endpoint;

  constructor(private router: Router,private http: HttpClient) { }

  ngOnInit(): void {
  }

  login() : void {
    this.http
      .get(this.endpoint.ANDJELA_DEV+'fitness/login/' + this.username + "/" + this.password)
      .pipe(
        map(returnedUser => {
            if(returnedUser != undefined)
                if(returnedUser["role"]=="admin"){
                  this.router.navigate(["loggedAdmin"]);
                }else if(returnedUser["role"]=="nutritionist"){
                  this.router.navigate(["loggedNutritionist"]);
                }else if(returnedUser["role"]=="trainer"){
                  this.router.navigate(["loggedTrainer"]);
                }else{
                  this.router.navigate(["loggedMember"]);
                }
              
              else
                alert("Invalid credentials")
        })
      ).subscribe()
      
  }

 

}


