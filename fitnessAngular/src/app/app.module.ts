import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';



import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDialogModule } from '@angular/material/dialog';

import { MatFormFieldModule } from '@angular/material/form-field';



import { GlobalUseService } from './services/global-use';


import {MatDividerModule} from '@angular/material/divider';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input'

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { MemberHomePageComponent } from './member-home-page/member-home-page.component';
import { NutritionistHomePageComponent } from './nutritionist-home-page/nutritionist-home-page.component';
import { TrainerHomePageComponent } from './trainer-home-page/trainer-home-page.component';
import { AdminHomePageComponent } from './admin-home-page/admin-home-page.component';


@NgModule({
  declarations: [
    AppComponent,

    MemberHomePageComponent,


    LoginComponent,
    SignupComponent,
    NutritionistHomePageComponent,
    TrainerHomePageComponent,
    AdminHomePageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatDividerModule,
    MatTableModule,
    MatCardModule,
    MatInputModule
  ],
  providers: [GlobalUseService],
  bootstrap: [AppComponent]
})

export class AppModule { }
