import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Endpoint } from '../util/endpoints-enum';

@Component({
  selector: 'app-member-home-page',
  templateUrl: './member-home-page.component.html',
  styleUrls: ['./member-home-page.component.css']
})
export class MemberHomePageComponent implements OnInit {

  constructor(private http: HttpClient) { }

  endpoint = Endpoint;

  ngOnInit(): void {
  }

}
